#pragma once

#define BLACK	"0"
#define BLUE	"1"
#define GREEN	"2"
#define AQUA	"3"
#define RED		"4"
#define PURPLE	"5"
#define YELLOW	"6"
#define WHITE	"7"
#define	GRAY	"8"
#define	LBLUE	"9"
#define	LGREEN	"A"
#define	LAQUA	"B"
#define	LRED	"C"
#define	LPURPLE	"D"
#define	LYELLOW	"E"
#define BWHITE	"F"

enum ConsoleColor
{
	Black = 0,
	Blue = 1,
	Green = 2,
	Cyan = 3,
	Red = 4,
	Magenta = 5,
	Brown = 6,
	LightGray = 7,
	DarkGray = 8,
	LightBlue = 9,
	LightGreen = 10,
	LightCyan = 11,
	LightRed = 12,
	LightMagenta = 13,
	Yellow = 14,
	White = 15
};