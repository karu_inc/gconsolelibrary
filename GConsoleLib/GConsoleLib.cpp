#include "GConsole.h"

void test1();
void test2();

int main()
{
	test2();
    return 0;
}

void test1()
{
	GConsole console({ 160, 50 }, Black, Yellow);

	console.SetRusLocale();

	cout << "Hello World!" << endl;
	Sleep(3000);

	cout << "Console colors:" << endl;
	cout << console.GetBackgroundColor() << endl;
	cout << console.GetTextColor() << endl;

	console.Write("Test1", true, Yellow, Blue);
	console.Write("Test2", true, Blue, White);
	console.Write("Test3", true, Green, White);

	Sleep(1000);

	console.Write("Test4", true);

	Sleep(1000);
	int a = 1;
	float b = 0;
	string c;

	console.Read(a);
	console.Read(b, Yellow);
	console.Read(c, White, Green);
	for (int i = 0; i < 20; i++)
	{
		console.Write("00000000", false);
	}

	Sleep(60000);
}

void test2()
{
	GConsole console({ 160, 50 }, White, Black);
	ConsoleColor colors[2] = { Red, LightRed };

	vector<string> v;
	string s;
	ifstream file("frames.txt");
	
	int k = 0;

	int columns = console.GetWindowWidth() - 1;
	int rows = console.GetWindowHeight() - 1;

	int i, j;

	string** matrix = new string*[rows];

	while (getline(file, s))
		v.push_back(s);
	file.close();

	for (i = 0; i < rows; i++)
		for (j = 0; j < columns; i++)
			cout << v[i][j];

	for (i = 0; i < rows; i++)
	{
		matrix[i] = new string[columns];
	}
		
	for (i = 0; i < rows; i++)
		for (j = 0; j < columns; j++)
		{
			matrix[i][j] = v[i][j];
		}
		
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < columns; j++)
		{
			if (k > 1)
			{
				k = 0;
			}

			console.Write(matrix[i][j], false, colors[k]);
			k++;
		}

		console.Write("", true);
	}
	Sleep(15000);
	/*
	while (true)
	{
		console.Clear();
		
		Sleep(1000);
	}*/
}

