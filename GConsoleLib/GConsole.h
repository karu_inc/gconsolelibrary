#pragma once
#include <stdio.h>
#include <tchar.h>
#include <windows.h>
#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>

#include "Colors.h"

using namespace std;

class GConsole
{
public:
	GConsole(COORD windowSize, ConsoleColor textCol = White, ConsoleColor backgroundCol = Black);

	void SetBackgroundColor(ConsoleColor colorID);
	void SetTextColor(ConsoleColor colorID);
	void SetRusLocale();

	string GetBackgroundColor();
	string GetTextColor();
	int GetWindowWidth();
	int GetWindowHeight();
	COORD GetWindowSize();

	void Write(int var, bool inLine = false, ConsoleColor textCol = White, ConsoleColor holdCol = Black);
	void Write(float var, bool inLine = false, ConsoleColor textCol = White, ConsoleColor holdCol = Black);
	void Write(string var, bool inLine = false, ConsoleColor textCol = White, ConsoleColor holdCol = Black);

	int Read(int var, ConsoleColor textCol = White, ConsoleColor holdCol = Black);
	float Read(float var, ConsoleColor textCol = White, ConsoleColor holdCol = Black);
	string Read(string var, ConsoleColor textCol = White, ConsoleColor holdCol = Black);

	void Clear();
private:
	string backgroundColor = BLACK;
	string textColor = WHITE;
	short windowWidth = 80;
	short windowHeight = 25;
	COORD windowSize = { windowWidth, windowHeight };
	HANDLE consoleOut = GetStdHandle(STD_OUTPUT_HANDLE);


	map<ConsoleColor, string> colors =
	{
		{ Black, BLACK },
		{ Blue, BLUE },
		{ Green, GREEN },
		{ Cyan, AQUA },
		{ Red, RED },
		{ Magenta, PURPLE },
		{ Brown, YELLOW },
		{ LightGray, WHITE },
		{ DarkGray, GRAY },
		{ LightBlue, LBLUE },
		{ LightGreen, LGREEN },
		{ LightCyan, LAQUA },
		{ LightRed, LRED },
		{ LightMagenta, LPURPLE },
		{ Yellow, LYELLOW },
		{ White, BWHITE }
	};
	void SetWindowSize(short width, short height);
};