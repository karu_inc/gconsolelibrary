#include "GConsole.h"

GConsole::GConsole(COORD windowSize, ConsoleColor textCol, ConsoleColor backgroundCol)
{
	SetWindowSize(windowSize.X, windowSize.Y);
	SetBackgroundColor(backgroundCol);
	SetTextColor(textCol);
}

void GConsole::SetBackgroundColor(ConsoleColor colorID)
{
	backgroundColor = colors[colorID];
	string color = "color " + colors[colorID] + textColor;
	system(color.c_str());
}

void GConsole::SetTextColor(ConsoleColor colorID)
{
	textColor = colorID;
	string color = "color " + backgroundColor + colors[colorID];
	system(color.c_str());
}

void GConsole::SetWindowSize(short width, short height)
{
	string command = "mode " + to_string(width) + "," + to_string(height);
	system(command.c_str());
	windowWidth = width;
	windowHeight = height;
}

void GConsole::SetRusLocale() 
{
	setlocale(LC_ALL, "Russian");
}

string GConsole::GetBackgroundColor()
{
	return backgroundColor;
}

string GConsole::GetTextColor()
{
	return textColor;
}

int GConsole::GetWindowWidth()
{
	return windowWidth;
}

int GConsole::GetWindowHeight()
{
	return windowHeight;
}

COORD GConsole::GetWindowSize()
{
	return windowSize;
}

void GConsole::Write(int var, bool inLine, ConsoleColor textColor, ConsoleColor holdColor)
{
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((holdColor << 4) | textColor));

	inLine ? cout << var << endl : cout << var;
}

void GConsole::Write(float var, bool inLine, ConsoleColor textColor, ConsoleColor holdColor)
{
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((holdColor << 4) | textColor));

	inLine ? cout << var << endl : cout << var;
}

void GConsole::Write(string var, bool inLine, ConsoleColor textColor, ConsoleColor holdColor)
{
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((holdColor << 4) | textColor));

	inLine ? cout << var << endl : cout << var;
}

int GConsole::Read(int var, ConsoleColor textColor, ConsoleColor holdColor)
{
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((holdColor << 4) | textColor));

	cin >> var;

	return var;
}

float GConsole::Read(float var, ConsoleColor textColor, ConsoleColor holdColor)
{
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((holdColor << 4) | textColor));

	cin >> var;

	return var;
}

string GConsole::Read(string var, ConsoleColor textColor, ConsoleColor holdColor)
{
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((holdColor << 4) | textColor));

	cin >> var;

	return var;
}

void GConsole::Clear()
{
	system("cls");
}